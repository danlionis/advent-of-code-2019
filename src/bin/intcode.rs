extern crate aoc19;

use aoc19::intcode::Computer;
use std::env::args;

fn main() {
    let args: Vec<String> = args().collect();

    if args.len() < 2 {
        panic!("memory required");
    }

    let mem: Vec<isize> = args[1]
        .split(",")
        .filter_map(|op| op.parse().ok())
        .collect();

    let mut computer = Computer::with_mem(&mem);

    computer.run(vec![], |out| {
        println!("[OUT] {}", out as u8 as char);
    });
}
